package com.pantip.android.prefstate.wrapper

import android.graphics.Bitmap
import android.os.Bundle

/**
 * Adopted from https://github.com/livefront/bridge
 */
object WrapperUtils {

    fun unwrapOptimizedObjects(bundle: Bundle) {
        val keys = bundle.keySet()
        for (key in keys) {
            if (bundle.get(key) is BitmapWrapper) {
                val bitmapWrapper = bundle.get(key) as BitmapWrapper

                bundle.putParcelable(key, bitmapWrapper.bitmap)
            }
        }
    }

    fun wrapOptimizedObjects(bundle: Bundle) {
        val keys = bundle.keySet()
        for (key in keys) {
            if (bundle.get(key) is Bitmap) {
                val bitmap = bundle.get(key) as Bitmap

                bundle.putParcelable(key, BitmapWrapper(bitmap))
            }
        }
    }

}