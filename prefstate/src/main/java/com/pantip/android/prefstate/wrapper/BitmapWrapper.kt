package com.pantip.android.prefstate.wrapper

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Parcel
import android.os.Parcelable
import java.io.ByteArrayOutputStream


/**
 * Adopted from https://github.com/livefront/bridge
 *
 * A wrapper class for a [Bitmap] that can be placed into a [android.os.Bundle] that
 * may be written to disk.
 */
internal class BitmapWrapper : Parcelable {

    var bitmap: Bitmap? = null
        private set

    constructor(bitmap: Bitmap) {
        this.bitmap = bitmap
    }

    //region Parcelable
    protected constructor(`in`: Parcel) {
        val bytes = `in`.createByteArray()
        bitmap = BitmapFactory.decodeByteArray(
                bytes,
                0,
                bytes.size)
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        val stream = ByteArrayOutputStream()
        bitmap!!.compress(Bitmap.CompressFormat.PNG, 100, stream)
        dest.writeByteArray(stream.toByteArray())
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<BitmapWrapper> = object : Parcelable.Creator<BitmapWrapper> {
            override fun createFromParcel(`in`: Parcel): BitmapWrapper {
                return BitmapWrapper(`in`)
            }

            override fun newArray(size: Int): Array<BitmapWrapper?> {
                return arrayOfNulls(size)
            }
        }
    }
    //endregion Parcelable

}