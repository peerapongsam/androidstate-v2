package com.pantip.android.prefstate

import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Parcel
import android.util.Base64
import com.pantip.android.prefstate.wrapper.WrapperUtils
import java.util.*

internal class PrefStateDelegate(context: Context) {

    init {
        registerForLifecycleEvents(context)
    }

    companion object {
        private val KEY_BUNDLE = "bundle_%s"
        private val KEY_UUID = "uuid_%s"
        private val AUTO_DATA_CLEAR_INTERVAL_MS = 100
    }

    private val PREF_NAME = PrefStateDelegate::class.java.name

    private var isClearAllowed: Boolean = false
    private var isFirstRestoreCall: Boolean = true
    private var lastClearTime: Long = 0
    private var uuidBundleMap: MutableMap<String, Bundle> = mutableMapOf()
    private var objectUuidMap: MutableMap<Any, String> = mutableMapOf()
    private var recentUuids: MutableSet<String> = mutableSetOf()

    private val sharedPreferences: SharedPreferences by lazy {
        context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
    }

    private fun registerForLifecycleEvents(context: Context) {
        (context.applicationContext as Application).registerActivityLifecycleCallbacks(object : ActivityLifecycleCallbacksAdapter() {
            override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
                isClearAllowed = true
            }

            override fun onActivityDestroyed(activity: Activity) {
                // Don't allow clearing during known configuration changes
                isClearAllowed = !activity.isChangingConfigurations
            }
        }
        )
    }

    fun clear(target: Any) {
        if (!isClearAllowed) return

        val uuid = objectUuidMap.remove(target) ?: return
        clearDataForUuid(uuid)
    }

    fun clearAll() {
        recentUuids.clear()
        uuidBundleMap.clear()
        objectUuidMap.clear()
        sharedPreferences.edit().clear().apply()
    }

    private fun clearDataForUuid(uuid: String) {
        sharedPreferences.edit()
                .remove(getKeyForEncodedBundle(uuid))
                .apply()
    }

    private fun clearStateData() {
        val currentTime = System.currentTimeMillis()
        if (currentTime - lastClearTime < AUTO_DATA_CLEAR_INTERVAL_MS) {
            return
        }
        lastClearTime = currentTime
        System.gc()

        val staleUuids = recentUuids.toHashSet()
        staleUuids.removeAll(objectUuidMap.values)
        staleUuids.forEach { clearDataForUuid(it) }
    }

    private fun getKeyForEncodedBundle(uuid: String): String {
        return String.format(Locale.US, KEY_BUNDLE, uuid)
    }

    private fun getKeyForUuid(target: Any): String {
        return String.format(Locale.US, KEY_UUID, target.javaClass.name)
    }

    private fun readFromDisk(uuid: String): Bundle? {
        val encodedString = sharedPreferences.getString(getKeyForEncodedBundle(uuid), null) ?: return null
        val parcelBytes = Base64.decode(encodedString, 0)
        val parcel = Parcel.obtain()
        parcel.unmarshall(parcelBytes, 0, parcelBytes.size)
        parcel.setDataPosition(0)
        val bundle = parcel.readBundle(PrefStateDelegate::class.java.classLoader)
        parcel.recycle()
        return bundle
    }

    private fun writeToDisk(uuid: String, bundle: Bundle) {
        val parcel = Parcel.obtain()
        parcel.writeBundle(bundle)
        val encodedString = Base64.encodeToString(parcel.marshall(), 0)
        sharedPreferences.edit()
                .putString(getKeyForEncodedBundle(uuid), encodedString)
                .apply()
        parcel.recycle()
    }

    fun saveInstanceState(target: Any, savedState: Bundle, superState: Bundle) {
        var uuid = objectUuidMap[target]
        if (uuid == null) {
            uuid = UUID.randomUUID().toString()
            objectUuidMap.put(target, uuid)
        }
        superState.putString(getKeyForUuid(target), uuid)
        WrapperUtils.wrapOptimizedObjects(savedState)
        recentUuids.add(uuid)
        uuidBundleMap.put(uuid, savedState)
        writeToDisk(uuid, savedState)
        clearStateData()
    }

    fun restoreInstanceState(target: Any, superState: Bundle?): Bundle? {
        val firstRestore = isFirstRestoreCall
        isFirstRestoreCall = false
        if (superState == null) {
            if (firstRestore) {
                sharedPreferences.edit()
                        .clear()
                        .apply()
            }
            return null
        }
        val uuid: String = (if (objectUuidMap.containsKey(target))
            objectUuidMap[target]
        else
            superState.getString(getKeyForUuid(target), null)) ?: return null

        objectUuidMap.put(target, uuid)

        val savedState = (if (uuidBundleMap.containsKey(uuid))
            uuidBundleMap[uuid]
        else
            readFromDisk(uuid)) ?: return null

        WrapperUtils.unwrapOptimizedObjects(savedState)
        clearDataForUuid(uuid)
        return savedState
    }
}