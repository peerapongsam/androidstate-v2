package com.pantip.android.prefstate

import android.content.Context
import android.os.Bundle

/**
 * @author peerapongsam
 */
object PrefState {

    private var prefStateDelegate: PrefStateDelegate? = null

    private fun checkInitialization() {
        if (prefStateDelegate == null) {
            throw IllegalStateException(
                    "You must first call initialize before calling any other methods")
        }
    }

    fun clear(target: Any) {
        checkInitialization()
        prefStateDelegate!!.clear(target)
    }

    fun clearAll(context: Context) {
        val delegate = if (prefStateDelegate != null)
            prefStateDelegate
        else
            PrefStateDelegate(context)

        delegate?.clearAll()
    }

    fun initialize(context: Context) {
        prefStateDelegate = PrefStateDelegate(context)
    }

    fun restoreInstanceState(target: Any, state: Bundle?): Bundle? {
        checkInitialization()
        return prefStateDelegate?.restoreInstanceState(target, state)
    }

    fun saveInstanceState(target: Any, bundle: Bundle, outState: Bundle) {
        checkInitialization()
        prefStateDelegate?.saveInstanceState(target, bundle, outState)
    }
}
