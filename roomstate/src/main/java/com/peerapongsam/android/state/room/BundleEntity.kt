package com.peerapongsam.android.state.room

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "states")
open class BundleEntity(
        @PrimaryKey
        var id: String = "",
        var value: String = ""
)