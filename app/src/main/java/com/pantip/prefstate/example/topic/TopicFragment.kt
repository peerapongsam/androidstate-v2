package com.pantip.prefstate.example.topic

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import com.pantip.prefstate.example.R
import com.pantip.prefstate.example.mvp.BaseFragment
import kotlinx.android.synthetic.main.fragment_topic.*

/**
 * @author peerapongsam
 */
class TopicFragment : BaseFragment<TopicViewModel, TopicContract.View, TopicContract.Presenter>(), TopicContract.View {
    private val TAG = "TopicFragment"

    override val layoutResId: Int
        get() = R.layout.fragment_topic


    private var topicAdapter = TopicAdapter()

    override fun createPresenter(): TopicContract.Presenter {
        return TopicPresenter()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvTopicList.apply {
            adapter = topicAdapter
            layoutManager = LinearLayoutManager(context)
        }

        srlRefresher.setOnRefreshListener {
            presenter.fetchTopics()
        }

        if (savedInstanceState == null) {
            presenter.fetchTopics()
        }
    }

    override fun onViewModelChanged(viewModel: TopicViewModel) {
        Log.d(TAG, "onViewModelChanged() called with: viewModel = [$viewModel]")

        srlRefresher.isRefreshing = viewModel.loading

        topicAdapter.items = viewModel.items
    }
}