package com.pantip.prefstate.example.mvp

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.peerapongsam.android.state.room.RoomState
import org.parceler.Parcels

/**
 * @author peerapongsam
 */
abstract class BaseFragment<VM, V : BaseContract.View<VM>, out P : BaseContract.Presenter<VM, V>>
    : Fragment(), BaseContract.View<VM> {


    abstract val layoutResId: Int

    val presenter: P by lazy { createPresenter() }

    abstract fun createPresenter(): P

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {

        } else {
            /* Android State */
            //restoreInstanceState(savedInstanceState)

            /* PrefState */
            /*Bundle().also {
                PrefState.restoreInstanceState(this, savedInstanceState)?.let {
                    restoreInstanceState(it)
                }
            }*/

            /* RealmState */
            /*Bundle().also {
                RealmState.restoreInstanceState(this, savedInstanceState)?.let {
                    restoreInstanceState(it)
                }
            }*/

            /*RoomState*/
            Bundle().also {
                RoomState.restoreInstanceState(this, savedInstanceState)?.let {
                    restoreInstanceState(it)
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(layoutResId, container, false)
    }

    @Suppress("UNCHECKED_CAST")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this as V)
    }

    protected fun restoreInstanceState(savedInstanceState: Bundle) {
        presenter.restoreViewModel(Parcels.unwrap(savedInstanceState.getParcelable(presenter::class.java.simpleName)))
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        /* Android State */
        //saveInstanceState(outState)

        /* PrefState */
        /*Bundle().also {
            saveInstanceState(it)
            PrefState.saveInstanceState(this, it, outState)
        }*/

        /* RealmState */
        /* Bundle().also {
             saveInstanceState(it)
             RealmState.saveInstanceState(this, it, outState)
         }*/


        /*RoomState*/
        Bundle().also {
            saveInstanceState(it)
            RoomState.saveInstanceState(this, it, outState)
        }
    }

    protected fun saveInstanceState(outState: Bundle) {
        outState.putParcelable(presenter::class.java.simpleName, Parcels.wrap(presenter.currentViewModel))
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detachView()
    }

    override fun onDestroy() {
        super.onDestroy()
        //RealmState.clear(this)

        RoomState.clear(this)
    }
}