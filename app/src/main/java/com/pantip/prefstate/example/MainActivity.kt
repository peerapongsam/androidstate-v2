package com.pantip.prefstate.example

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v7.app.AppCompatActivity
import com.pantip.prefstate.example.topic.TopicFragment
import kotlinx.android.synthetic.main.activity_main.*

/**
 * @author peerapongsam
 */
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        if (savedInstanceState == null) {

        }
        val pageAdapter = MainPageAdapter(supportFragmentManager)

        page.apply {
            adapter = pageAdapter
            offscreenPageLimit = pageAdapter.count
        }
        tab.setupWithViewPager(page)
    }

}

class MainPageAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment? {
        return when (position) {
            0 -> TopicFragment()
            1 -> TopicFragment()
            2 -> TopicFragment()
            3 -> TopicFragment()
            4 -> TopicFragment()
            else -> null
        }
    }

    override fun getCount(): Int {
        return 5
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return "PAGE $position"
    }
}