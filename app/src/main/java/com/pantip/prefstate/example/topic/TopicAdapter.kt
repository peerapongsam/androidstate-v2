package com.pantip.prefstate.example.topic

import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pantip.prefstate.example.R
import kotlinx.android.synthetic.main.item_topic.view.*
import kotlin.properties.Delegates

/**
 * @author peerapongsam
 */
class TopicAdapter : RecyclerView.Adapter<TopicViewHolder>() {
    var items by Delegates.observable(listOf<TopicItemViewModel>()) { _, oldValue, newValue ->
        DiffUtil.calculateDiff(TopicDiffCallback(oldValue, newValue)).dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TopicViewHolder {
        return TopicViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_topic, parent, false))
    }

    override fun onBindViewHolder(holder: TopicViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }
}

class TopicDiffCallback(private val oldValue: List<TopicItemViewModel>,
                        private val newValue: List<TopicItemViewModel>) : DiffUtil.Callback() {
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldValue[oldItemPosition].id == newValue[newItemPosition].id
    }

    override fun getOldListSize(): Int {
        return oldValue.size
    }

    override fun getNewListSize(): Int {
        return newValue.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldValue[oldItemPosition] == newValue[newItemPosition]
    }

}

class TopicViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    fun bind(topicItemViewModel: TopicItemViewModel) {
        itemView.tvTitle.text = topicItemViewModel.title
        itemView.tvDescription.text = topicItemViewModel.description
    }
}
