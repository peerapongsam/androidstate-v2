package com.pantip.prefstate.example.topic

import com.pantip.prefstate.example.mvp.BasePresenter
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * @author peerapongsam
 */
class TopicPresenter : BasePresenter<TopicViewModel, TopicContract.View>(), TopicContract.Presenter {
    override fun defaultViewModel(): TopicViewModel {
        return TopicViewModel(loading = false, items = listOf())
    }

    override fun fetchTopics() {
        Observable.fromCallable {
            (5000 downTo 1)
                    .map {
                        TopicItemViewModel(
                                id = it,
                                title = "แชร์ประสบการณ์ลูกติดมือถือ จนต้องออกจากงานมาดูแลเอง $it",
                                description = ".สวัสดีค่ะเราจะมาแชร์ประสบการณ์ลูกชายติดมือถือ ติดแท็ปเล็ตจนไม่ยอมพูดค่ะเท้าความไปตั้งแต่ตอนท้อง เราอายุ 28 ไม่มีภาวะเสี่ยงใดๆ ฝากท้องปกติทุกอย่าง ตั้งใจไว้ว่าจะคลอดเอง เลี้ยงเอง โดยให้ $it",
                                author = "กอหญ้ากะฟ้าคราม $it",
                                imageCover = "https://ptcdn.info/pick/168/000/000/p0tvf0o88kAQGsH4OF3-s.jpg",
                                avatar = "https://p.ptcdn.info/220/008/000/636630_0102091256_14383-973337689350714-2034036395187843688-n_s.jpg",
                                comments = it
                        )
                    }
        }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map {
                    viewModel.copy(
                            loading = false,
                            items = it
                    )
                }
                .onErrorReturn {
                    viewModel.copy(
                            loading = false,
                            items = listOf()
                    )
                }
                .startWith(viewModel.copy(
                        loading = true
                ))
                .subscribe {
                    viewModel = it
                }
    }
}